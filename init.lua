-- rumble - Client side mod for Minetest
-- Scripted by: Li0n

-- configureable settings:
local rumble_strength_max_standard = 0.5
local rumble_duration_max_standard = 5

-- To do list:
-- sadist mode
-- tnt griefer mode
-- log player name only once, after player is initialized.
-- editable table of words to trigger notification rumble
-- command to choose a word/name that triggers notification rumble
-- notification mode: trigger rumble from specific words written by either yourself or others
-- condense on/off commands into single commands. eg .chatlog on and .chatlog off instead of .chatlog_on and .chatlog_off
-- make a GUI for all the settings
-- make a section in the code where all default settings can be changed


local rumble_strength_max = rumble_strength_max_standard
local rumble_duration_max = rumble_duration_max_standard
local log
local log_mode
local local_player_name
local notification_mode
local chat_trigger

local log_only = function(text)
  minetest.log("action", text)
end

local log_and_chat = function(text)
  minetest.log("action", text)
  minetest.display_chat_message(text)
end

local no_log_no_chat = function(text)
end

-- step 1 (start)
minetest.register_on_mods_loaded(function()
    minetest.display_chat_message("rumble mod loaded. May the rumble be with you!")
    log = log_and_chat
    log_mode = "log and chat"
end)

-- step 2 (take damage)
minetest.register_on_damage_taken(function(hp)
    local rumble_strength = math.min( hp/20, rumble_strength_max )
    local rumble_duration = math.min( hp, rumble_duration_max )
    log("[rumble] ".."queue "..rumble_strength.." "..rumble_duration)
end)

-- step 3 (die)
minetest.register_on_death(function()
    log("[rumble] instant 0 0")
end)

-- step 4 (disconnect)
minetest.register_on_shutdown(function()
    log("[rumble] instant 0 0")
end)

-- step 5 (chat mode)
minetest.register_on_receiving_chat_message(function(message)
    if notification_mode == true and (string.match((message), "^.*<.*>.*$")) and minetest.localplayer ~= nil then
        local_player_name = minetest.localplayer:get_name(self)
        if not (string.match((message), "^.*"..local_player_name..".*$")) then
            if chat_trigger == nil then
                log("[rumble] queue 0.2 5")
            else
                if (string.match((message), "^.*"..chat_trigger..".*$")) then
                    log("[rumble] queue 0.2 5")
                end
            end
        end
    end
end)

-- step 6 (item use test and node place tests)

--minetest.register_on_item_use(function(itemstack, pointed_thing)
--    log("itemstack: "..tostring(itemstack))
--    log("pointed thing: "..dump(pointed_thing))
--end)


-- step 7 (commands)

-- command 1
minetest.register_chatcommand("settings", {
    description = "shows current values of settings",
    func = function()
        minetest.display_chat_message("max rumble strength: "..rumble_strength_max)
        minetest.display_chat_message("max rumble duration: "..rumble_duration_max)
        minetest.display_chat_message("current chat and log mode: "..log_mode)
        minetest.display_chat_message("local player name: "..tostring(local_player_name))
            if notification_mode == true then
                minetest.display_chat_message("notification mode: on")
            elseif notification_mode == false then
                minetest.display_chat_message("notification mode: off")
            end

        minetest.display_chat_message("chat trigger: "..tostring(chat_trigger))
    end,
})

-- command 2
minetest.register_chatcommand("strength", {
    description = "set max rumble strength (from 0 to 1)",
    func = function(text)
        if (string.match((text), "^%d+%.?%d*$")) then
            rumble_strength_max = text
            minetest.display_chat_message("max rumble strength set to "..rumble_strength_max)
        else
            minetest.display_chat_message(minetest.colorize("#FF0", "error: value after .strength should be a positive number"))
        end
    end,
})

-- command 3
minetest.register_chatcommand("duration", {
    description = "set max rumble duration (in seconds)",
    func = function(text)
        if (string.match((text), "^%d+%.?%d*$")) then
            rumble_duration_max = text
            minetest.display_chat_message("max rumble duration set to "..rumble_duration_max)
        else
            minetest.display_chat_message(minetest.colorize("#FF0", "error: value after .duration should be a positive number"))
        end
    end,
})

-- command 4
minetest.register_chatcommand("chatlog_off", {
    description = "disables rumble logs being written into chat",
    func = function()
        log = log_only
        log_mode = "log only"
        minetest.display_chat_message("rumble chat turned off")
    end,
})

-- command 5
minetest.register_chatcommand("chatlog_on", {
    description = "enables rumble logs being written into chat",
    func = function()
        log = log_and_chat
        log_mode = "log and chat"
        minetest.display_chat_message("rumble chat turned on")
    end,
})

-- command 6
minetest.register_chatcommand("stop", {
    description = "pauses logging",
    func = function()
        log = no_log_no_chat
        log_mode = "no log no chat"
        minetest.display_chat_message("rumble logging paused")
    end,
})

-- command 7
minetest.register_chatcommand("resume", {
    description = "resumes the rumble program",
    func = function()
        log = log_only
        log_mode = "log only"
        minetest.display_chat_message("rumble logging resumed")
    end,
})

-- command 8
minetest.register_chatcommand("notifications_on", {
    description = "turns on notification mode: triggers rumble for every received message",
    func = function()
        notification_mode = true
        minetest.display_chat_message("notification mode: on")
    end,
})

-- command 9
minetest.register_chatcommand("notifications_off", {
    description = "turns off notification mode: does not trigger rumble for received messages",
    func = function()
        notification_mode = false
        minetest.display_chat_message("notification mode: off")
    end,
})

-- command 10
minetest.register_chatcommand("chat_trigger", {
    description = "if set, will only trigger notification rumbles for a specific word or name typed after the command",
    func = function(text)
        chat_trigger = text
        minetest.display_chat_message("notification trigger: "..tostring(chat_trigger))
    end,
})
