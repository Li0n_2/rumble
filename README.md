this clientside mod interprets and logs certain types of player interactions, so that these logs can be used by external programs.
it was made to be used in combination with a python program and Intiface to add rumble support to minetest.
for now it only reacts to player damage and death, writing a vibration strength and duration into the logs.

installing clientside mods is not very well documented and varies between clients.

this forum post explains it quite ok https://forum.minetest.net/viewtopic.php?f=53&t=17830

1) Navigate to your minetest directory and create a directory (aka a folder) called clientmods if it does not exist.

2) Download or clone the mod into the clientmods directory.

3) Create a text file called mods.conf inside the clientmods directory if it does not exist. this should be in the same directory that has debug.txt in it.

4) Open mods.conf in your favorite text editor and add the line "load_mod_rumble = true" (without the quotation marks). Save the file.

5) Open Minetest and navigate to the Settings tab. Select All Settings. Navigate to Client > Network > Client modding or search for "client". With Client modding selected, select the Edit button and change the displayed option to Enabled in the dropdown, or simply double-click the setting. Save your changes. Note: on some clients this setting my be called enable_client_modding instead of Client modding.

Your client-side mod has been enabled!
Type .help all in chat to see what you can do!

-------------

installation instructions for the external programs that are intended to be used with this clientside mod:

1) make sure you have python 3 installed. for windows users I recommend installing it through the microsoft store: https://www.microsoft.com/store/productId/9PJPW5LDXLZ5

2) open a command prompt.

3) type "pip install buttplug", press enter, and wait. this will install the buttplug library needed to run this program.

4) make sure you have Intiface installed to connect to your favourite rumble device. you can install it from here: https://intiface.com/desktop

--------------

How to run the programs as intended:

1) start intiface and connect your rumble device. intiface tutorials can be found on their website https://intiface.com/desktop

2) open a command prompt.

3) in the command prompt, navigate to the directory that has the minetest_intiface.py file in it. this should be in the rumble mod. if you've never used a command line before I recommend asking a friend or google how to do this.

3) type "python3 minetest_intiface.py" and press enter to start running the python code from the minetest directory.

4) start minetest.

5) join or start a server to start playing.

Enjoy!